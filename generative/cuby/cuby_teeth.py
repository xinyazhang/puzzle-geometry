#!/usr/bin/env python


'''
This script generate the blender script that create the cubes for boolean
operations in order to create gaps in the box.
Collection "Teeth" in Scene "Fixer" from CubyComplete.blend is generated from
this script.
'''

import numpy as np
from collections import namedtuple

X = np.array([1, 0, 0])
Y = np.array([0, 1, 0])
Z = np.array([0, 0, 1])
O = np.array([0, 0, 0])

TeethPlace = namedtuple('TeethPlace', ['full_move', 'half_move'])

FaceDescrption = namedtuple('FaceDescrption', ['coord', 'teeth_desc'])

TEETH_PLACES = [
    TeethPlace(full_move='Y', half_move='-X'),
    TeethPlace(full_move='Y', half_move='X'),
    TeethPlace(full_move='X', half_move='Y'),
    TeethPlace(full_move='X', half_move='-Y'),
    TeethPlace(full_move='-Y', half_move='X'),
    TeethPlace(full_move='-Y', half_move='-X'),
    TeethPlace(full_move='-X', half_move='-Y'),
    TeethPlace(full_move='-X', half_move='Y'),
]

TEETH_PLACE_MOVE_CODE = {
    'X'  : lambda x,y: x,
    '-X' : lambda x,y: -x,
    'Y'  : lambda x,y: y,
    '-Y' : lambda x,y: -y,
}

# 1: 4.30mm gap
# 2: 6.0mm gap
# 0: No gap
FACE_1 = FaceDescrption(
    coord=(X, Y, Z), # Coordinate system
    teeth_desc=[1,1, 0,1, 1,1, 1,0] # Clockwise, from "top" edge
)

FACE_2 = FaceDescrption(
    coord=(X, Z, -Y), # Coordinate system
    teeth_desc=[0,1, 1,1, 0,1, 1,1] # Clockwise, from "top" edge
)

FACE_3 = FaceDescrption(
    coord=(Y, Z, X), # Coordinate system
    teeth_desc=[1,1, 1,0, 1,1, 1,0] # Clockwise, from "top" edge
)

FACE_4 = FaceDescrption(
    coord=(-Y, Z, -X), # Coordinate system
    teeth_desc=[2,1, 1,0, 1,1, 1,1] # Clockwise, from "top" edge
)

FACE_5 = FaceDescrption(
    coord=(-X, Z, Y), # Coordinate system
    teeth_desc=[0,1, 1,1, 0,1, 1,1] # Clockwise, from "top" edge
)

FACE_6 = FaceDescrption(
    coord=(X, -Y, -Z), # Coordinate system
    teeth_desc=[1,1, 0,1, 1,1, 1,0] # Clockwise, from "top" edge
)

FACES = [FACE_1, FACE_2, FACE_3, FACE_4, FACE_5, FACE_6]

SOLID_CUBE_HEIGHT = 38.44
HALF_SOLID_CUBE_HEIGHT = SOLID_CUBE_HEIGHT / 2.
HOLLOW_CUBE_SIZE = 23.00
HALF_HOLLOW_CUBE_SIZE = HOLLOW_CUBE_SIZE / 2.

HOLLOW_EDGE = 5.3
HOLLOW_GAP = 4.30
WIDE_HOLLOW_GAP = 6.
HOLLOW_TOOTH = 3.8
HOLLOW_DEPTH = 2.9

METAL_THICHNESS = 1.98

assert HOLLOW_EDGE * 2. + HOLLOW_GAP * 2. + HOLLOW_TOOTH == HOLLOW_CUBE_SIZE

def main():
    for face in FACES:
        x, y, z = face.coord
        for teeth_index, teeth_code in enumerate(face.teeth_desc):
            if teeth_code == 0:
                continue

            teeth_place = TEETH_PLACES[teeth_index]
            teeth_full_move = TEETH_PLACE_MOVE_CODE[teeth_place.full_move](x, y)
            teeth_half_move = TEETH_PLACE_MOVE_CODE[teeth_place.half_move](x, y)

            center = O + z * HALF_SOLID_CUBE_HEIGHT + teeth_full_move * HALF_HOLLOW_CUBE_SIZE
            if teeth_code == 1:
                gap_half_width = HOLLOW_GAP / 2.
            elif teeth_code == 2:
                gap_half_width = WIDE_HOLLOW_GAP / 2.
            else:
                assert False
            center += teeth_half_move * (HOLLOW_TOOTH / 2. + gap_half_width)
            scale = z * METAL_THICHNESS * 2. + teeth_full_move * HOLLOW_DEPTH + teeth_half_move * gap_half_width
            scale = np.abs(scale)
            print(f"bpy.ops.mesh.primitive_cube_add(location=({center[0]}, {center[1]}, {center[2]}), scale=({scale[0]}, {scale[1]}, {scale[2]}))")

        print()

if __name__ == '__main__':
    main()

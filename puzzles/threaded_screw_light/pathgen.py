#!/usr/bin/env python3

N=16.2 # Note: through trial and error, because the closed form doesn't work...
L=0.1247994843006639 - (-0.12020051569933576)

L_PER_2PI = L / N
L_PER_PI_OVER_2 = L_PER_2PI / 4.0
QUATS = [[1,0.0,0.0,0.0],
         [0.7071068,0.0,0.0,0.7071068],
         [0.0,0.0,0.0,1.0],
         [-0.7071068,0.0,0.0,0.7071068]]
Z0=0.004
ROTATION_TO_GENERATE = int(1.0/L * N + 1)

for i in range(ROTATION_TO_GENERATE):
    for r in range(4):
        x,y,z =[0.0, 0.0, Z0 + L_PER_2PI * i + L_PER_PI_OVER_2 * r]
        qw, qx, qy, qz = QUATS[r]
        print(f'{x} {y} {z} {qw} {qx} {qy} {qz}')

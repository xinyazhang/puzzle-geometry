#!/usr/bin/env python3

import math
import sys
import numpy as np
from scipy.spatial.transform import Rotation

'''
For pico: N = 7.23 is a feasible value
'''
N=float(sys.argv[1]) # Note: through trial and error, because the closed form doesn't work...
L=0.1247994843006639 - (-0.12020051569933576)
SQRT2 = math.sqrt(1.0/2.0)
O = np.array([-1.34136004e-03, -1.31900434e-03, 0.0])

L_PER_2PI = L / N
L_PER_PI_OVER_2 = L_PER_2PI / 4.0
'''
W-first
'''
QUATS = [[   1.0, 0.0, 0.0,   0.0],
         [ SQRT2, 0.0, 0.0, SQRT2],
         [   0.0, 0.0, 0.0,   1.0],
         [-SQRT2, 0.0, 0.0, SQRT2]]
Z0=-0.09
ROTATION_TO_GENERATE = int(1.0/L * N + 1)

def translate_vanilla_to_ompl(x,y,z,qx,qy,qz,qw):
    R = Rotation.from_quat([qx,qy,qz,qw]) # w-last
    t = np.array([x,y,z]) + R.apply(O)
    return t

for i in range(ROTATION_TO_GENERATE):
    for r in range(4):
        # x,y,z =[0.0, 0.0, Z0 + L_PER_2PI * i + L_PER_PI_OVER_2 * r]
        x,y,z =[1.31e-03, 0.925e-03, Z0 + L_PER_2PI * i + L_PER_PI_OVER_2 * r]
        qw, qx, qy, qz = QUATS[r]
        '''
        Note: this VANILLA is the version that picks [0,0, OMPL_Z] as reference point
        '''
        # x,y,z = translate_vanilla_to_ompl(x,y,z,qx,qy,qz,qw)
        print(f'{x} {y} {z} {qx} {qy} {qz} {qw}')

'''
From experiments, the closed from solution path is:
    0. Takes OMPL configuration space
       + Reference point ("robot shift" in OMPL code): [-1.34136004e-03, -1.31900434e-03, 2.09382868e+00]
    1. Starting from [0.00131, 0.000925, -0.09], Identity rotation
    2. Rotation Axis: Z
    3. Translation speed per 2Pi Rotation is: 0.03388658367911475. (Denoted as LoN)

    Thus we have closed form trajectory:
        + Translation: [0.00131, 0.000925, -0.09 + tau * LoN]
        + Rotation: [0, 0, 2Pi] * tau
'''

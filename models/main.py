
# -*- coding: utf-8 -*-

from lib import curve, polycurve, pipefuser
import argparse
import pymesh as pm
import numpy as np
import math

O = np.array([0,0,0], dtype=np.float64)
X = np.array([1,0,0], dtype=np.float64)
Y = np.array([0,1,0], dtype=np.float64)
Z = np.array([0,0,1], dtype=np.float64)
DEG2RAD = 1.0 / 180.0 * math.pi

def test():
    curves = [polycurve.Linear(np.array([0,0,0]), np.array([0,0,1]), Y),
              polycurve.Linear(np.array([0,0,0]), np.array([0,0,1]), Y)]
    V, F = pipefuser.fuse_curves(curves, 0.2, 0.125, 16)
    pm.save_mesh('test.obj', pm.form_mesh(V, F))
    cubic = polycurve.Cubic(x0=np.array([0,0,0]), x0prime=np.array([5,0,0]),
                            x1=np.array([0,2.5,0]), x1prime=np.array([-5,0,0]),
                            normal_hint=Y)
    curves = [#polycurve.Linear(np.array([0,0,0]), np.array([0,0,1]), Y),
              cubic]
    V, F = pipefuser.fuse_curves(curves, 0.2, 0.125, 16)
    pm.save_mesh('test2.obj', pm.form_mesh(V, F))

    cubic_2d_with_lin_h = polycurve.Cubic2DWithLinearHeight(x0=np.array([0,0,0]), x0prime=np.array([5,0,0]),
                            x1=np.array([0,2.5,0.5]), x1prime=np.array([-5,0,0]),
                            normal_hint=Y, height_index=2)
    curves = [cubic_2d_with_lin_h]
    V, F = pipefuser.fuse_curves(curves, 0.2, 0.125, 16)
    pm.save_mesh('test3.obj', pm.form_mesh(V, F))

def test2():
    c = polycurve.CircleXY(2, -0.5 * math.pi, math.pi * 1.5, 0, 4.0)
    print("c(0): {}".format(c.get_conf(0)))
    l = polycurve.Linear(O, O + X * 2.0, Z)
    curves = [c, l]
    V, F = pipefuser.fuse_curves(curves, 0.2, 0.125, 16)
    pm.save_mesh('test.obj', pm.form_mesh(V, F))

'''
⍺ with z like tail
'''
def curve_az():
    r0 = 4.40/2
    Ht = 20.26
    zangle = np.mean([37.62, 37.38, 36.86])
    # zangle = 10.0
    zrad = zangle * DEG2RAD
    aangle = np.mean([13.54, 13.67, 13.46])
    arad = aangle * DEG2RAD

    Zt_cross = np.mean([26.15,26.09])
    Z_cross = Zt_cross - Ht - r0 * 2
    Zt_circend = 31.20
    Z_circend = Zt_circend - Ht - r0 * 2
    Zt_end = np.mean([34.27,34.38, 34.46])
    Z_end = Zt_end - Ht - r0 * 2
    crv_segments = []

    l0 = polycurve.Linear(O, O + X * (34.00 - 6.47), Z)
    l1_x = (34.00 - 6.47 - 8.00)
    l1 = polycurve.Linear(O, O - l1_x * X + l1_x * math.tan(zrad) * Y , Z)
    '''
    c0t0 = l0.get_conf(1.0)[2]
    c0t1 = l1.get_conf(0.0)[2]
    print("c0 tans {} {}".format(c0t0, c0t1))
    c0 = polycurve.CircleXY(radius=6.47-r0,
                            x0rad = -0.5 * math.pi,
                            x1rad = 0.0,
                            h0 = 0.0, h1 = 0.0)
    c0c = c0.get_conf(1.0)
    c1 = polycurve.Cubic(x0 = c0c[0], x0prime=c0c[2],
                         x1 = O + Y * (r0 + 7.0 + r0/math.sin(math.pi/2 - zrad) - (6.47 - r0)), x1prime=c0t1,
                         normal_hint=Z)
    '''
    c0 = polycurve.CircleXY(radius=6.47-r0,
                            x0rad=-0.5 * math.pi,
                            x1rad=0.5 * math.pi - zrad,
                            h0=0.0, h1=0.0)
    '''
    print("ZTangent {}".format(ZTangent))
    crv_segments.append(polycurve.Cubic(x0=O, x0prime=X * 1.0,
                                        x1=O + X * (6.47 - r0) + Y * (7.00 / 2.0 + r0), x1prime=Y * ZTangent,
                                        normal_hint=Z))
    crv_segments.append(polycurve.Cubic(x0=O + X * (6.47 - r0) + Y * (7.00 / 2.0 + r0), x0prime=Y * ZTangent,
                                        x1=O + Y * (r0 + 7.0 + r0/math.sin(math.pi/2 - zrad)), x1prime= (X * -math.cos(zrad) + Y * math.sin(zrad)) * 1.0,
                                        normal_hint=Z))
    '''
    crv_segments.append(l0)
    crv_segments.append(c0)
    crv_segments.append(l1)
    '''
    crv_segments.append(polycurve.CircleXY(radius=(19.56+2.0*r0)/2.0,
                                           x0rad=math.pi-zrad,
                                           x1rad=0.0,
                                           h0=Z_cross,
                                           h1=0.0))
    crv_segments.append(polycurve.CircleXY(radius=(19.56+2.0*r0)/2.0,
                                           x0rad=0.0,
                                           x1rad=-math.pi+arad,
                                           h0=0.0,
                                           h1=Z_end/2))
    '''
    crv_segments.append(polycurve.CircleXY(radius=(19.56+2.0*r0)/2.0,
                                           x0rad=math.pi-zrad,
                                           x1rad=-math.pi+arad,
                                           h0=0.0,
                                           h1=Z_circend))
    crv_segments.append(polycurve.Linear(O, O + X * (17.04), Z))
    V, F = pipefuser.fuse_curves(crv_segments, r0, 0.125, 64)
    pm.save_mesh('az.obj', pm.form_mesh(V, F))

'''
⍺ with J like tail
'''
def curve_aj():
    d0 = np.mean([4.38, 4.36, 4.38, 4.35])
    d0_px = np.mean([183, 196, 192, 189])
    r0 = d0/2.0
    yr = np.mean([28.78, 28.77,28.78])
    yr_px = np.mean([1272,1264,1278])
    px2mm = np.mean([d0/d0_px, yr/yr_px])
    lb = 536 * px2mm
    lb_rad = np.mean([7.47, 7.61]) * DEG2RAD
    lj_radius = 2264 / 2.0 * px2mm
    lm = 1104.0 * px2mm
    lm_rad = 0.32 * DEG2RAD
    #le = np.mean([851.7, 819.0, 830.5]) * px2mm # Should not trust the pixels because it's closer to the camera
    le = np.mean([18.12, 18.86, 18.60])
    le_rad = np.mean([43.08, 43.29]) * DEG2RAD
    H_t = np.mean([20.25, 20.24, 20.25, 20.24])
    Z_be = np.mean([32.39, 32.40, 32.43])
    Z_e = Z_be - H_t - d0

    crv_segments = []

    crv_segments.append(polycurve.Linear(O, O + X * lb, Z))
    crv_segments.append(polycurve.CircleXY(radius=lj_radius, x0rad=0, x1rad=math.pi/2.0-lb_rad-lm_rad, h0=0.0, h1=0.0))
    crv_segments.append(polycurve.Linear(O, O + X * lm, Z))
    '''
    proto_0 = polycurve.CircleXY(radius=(yr-d0)/2.0, x0rad=0.0, x1rad=2*math.pi-le_rad, h0=0.0, h1=0.0)
    proto_1 = polycurve.Linear(O, O + X*le, Z)
    l_0 = proto_0.length
    l_1 = proto_1.length
    h_0 = l_0 / (l_0 + l_1) * Z_e # Height at the end of the arc
    print("Z_e {} h_0 {}".format(Z_e, h_0))
    '''
    crv_segments.append(polycurve.CircleXY(radius=(yr-d0)/2.0, x0rad=0.0, x1rad=2*math.pi-le_rad, h0=0.0, h1=-Z_e, zero_boudary_dh=True))
    crv_segments.append(polycurve.Linear(O, O + X * (lm - 7.5), Z))
    V, F = pipefuser.fuse_curves(crv_segments, r0, 0.125, 64)
    pm.save_mesh('aj.obj', pm.form_mesh(V, F))

'''
⍺ with g (double-storey lowercase) like tail
P.S. I'm trying not to use '8' to refer this puzzle because the head is much larger than the body
'''
def curve_ag():
    d0 = np.mean([4.48, 4.50, 4.47])
    r0 = d0/2.0
    l1 = 18.22
    dc1 = np.mean([28.42, 28.43, 28.36]) - r0 * 2
    rc1 = dc1/2.0
    c1rad0 = 0.0
    c1rad1 = -math.pi
    c1rad2 = -1.5 * math.pi - np.mean([22.61,22.66]) * DEG2RAD
    l2 = np.mean([20.20, 20.72, 20.96])
    c2rad0 = 0.5 * math.pi - np.mean([22.61,22.66]) * DEG2RAD
    c2rad1 = 2.0 * math.pi - np.mean([5.89, 6.06]) * DEG2RAD
    # dc2 = np.mean([19.60,19.56,19.52]) - r0 * 2
    dc2 = np.mean([19.60,19.56,19.52]) - r0 * 2 - 0.04 # minus 0.02 to make sure it fits
    rc2 = dc2/2.0
    l3 = np.mean([8.34, 8.35])
    H_t = np.mean([20.25, 20.24, 20.25, 20.24])
    Z_1 = 36.63 - H_t # do NOT minus d0 so that the height is sufficient visually
    crv_segments = []

    '''
    crv_segments.append(polycurve.Linear(O, O + X * l1, Z))
    crv_segments.append(polycurve.CircleXY(radius=rc1, x0rad=c1rad0, x1rad=c1rad1, h0=0.0, h1=0.0))
    scale = 1.0
    while True:
        print("Guessing h1 {}".format(scale))
        proto_0 = polycurve.CircleXY(radius=rc1, x0rad=c1rad1, x1rad=c1rad2, h0=0.0, h1=scale)
        l_0 = proto_0.length
        c_0 = proto_0.get_conf(1.0)
        p_0, t_0 = c_0[0], c_0[2]
        new_scale = Z_1 / (curve.r3normalize(t_0) * l2 + p_0)[2]
        if abs(new_scale - 1.0) < 1e-3:
            break
        scale *= new_scale

    z_0 = scale
    # proto_1 = polycurve.Linear(O, O + X * l2, Z)
    # z_0 = Z_1 * (proto_0.length / (proto_0.length + proto_1.length))
    # z_0 = 29.51 - H_t - d0
    print("z_0 {}".format(z_0))
    crv_segments.append(polycurve.CircleXY(radius=rc1, x0rad=c1rad1, x1rad=c1rad2, h0=0.0, h1=z_0))
    crv_segments.append(polycurve.Linear(O, O + X * l2, Z))
    crv_segments.append(polycurve.CircleXY(radius=rc2, x0rad=c2rad0, x1rad=c2rad1, h0=0.0, h1=d0, zero_boudary_dh=True)) # 0.75 is picked so the ending touches the rod
    crv_segments.append(polycurve.Linear(O, O + X * l3, Z))
    '''
    crv_segments.append(polycurve.Linear(O, O + X * l1, Z))
    crv_segments.append(polycurve.CircleXY(radius=rc1, x0rad=c1rad0, x1rad=c1rad2, h0=0.0, h1=(32.32 - H_t - d0), zero_boudary_dh=True))
    crv_segments.append(polycurve.Linear(O, O + X * l2, Z))
    crv_segments.append(polycurve.CircleXY(radius=rc2, x0rad=c2rad0, x1rad=c2rad1, h0=0.0, h1=d0, zero_boudary_dh=True)) # 0.75 is picked so the ending touches the rod
    crv_segments.append(polycurve.Linear(O, O + X * l3, Z))
    V, F = pipefuser.fuse_curves(crv_segments, r0, 0.125, 64)
    pm.save_mesh('ag.obj', pm.form_mesh(V, F))

def main():
    # test()
    # test2()
    curve_az()
    curve_aj()
    curve_ag()

if __name__ == '__main__':
    main()

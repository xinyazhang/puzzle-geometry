#!/usr/bin/env python3

import numpy as np
import curve
import scipy.linalg as spla
import math

def orthogonal_component(vector, tangent):
    vector -= tangent * np.dot(vector, tangent)
    return curve.r3normalize(vector)

class Linear(curve.Curve):
    '''
    Input:
        x0: x(0)
        x1: x(1)
        normal_hint: a vector that roughly indicates the normal direction
    '''
    def __init__(self, x0, x1, normal_hint):
        super(Linear, self).__init__()
        self._x0 = x0
        self._x1 = x1
        self._len = np.linalg.norm(x1 - x0)
        print("self._len {}".format(self._len))
        self._tangent = (x1 - x0) / self._len
        self._n = orthogonal_component(normal_hint[:3], self._tangent)

    def get_conf(self, tau):
        return tau * self._x1 + (1-tau) * self._x0, self._n, self._tangent

    @property
    def length(self):
        return self._len

class Cubic(curve.Curve):
    def __init__(self,
                 x0, x0prime,
                 x1, x1prime,
                 normal_hint):
        super(Cubic, self).__init__()
        self._x0 = x0
        self._x0p = x0prime
        self._x1 = x1
        self._x1p = x1prime
        self._nhint = normal_hint
        self._len = curve.numeric_length(self, 1024)

    def update_tangent(self, x0prime=None, x1prime=None):
        if x0prime is not None:
            self._x0p = x0prime
        if x1prime is not None:
            self._x1p = x1prime
        self._len = curve.numeric_length(self, 1024)

    def get_conf(self, tau):
        tau3 = tau**3.0
        tau2 = tau**2.0
        x = (2 * tau3 - 3 * tau2 + 1) * self._x0
        x += (tau3 - 2 * tau2 + tau) * self._x0p
        x += (-2 * tau3 + 3 * tau2) * self._x1
        x += (tau3 - tau2) * self._x1p
        xp = (6 * tau2 - 6 * tau) * self._x0
        xp += (3 * tau2 - 4 * tau + 1) * self._x0p
        xp += (-6 * tau2 + 6 * tau) * self._x1
        xp += (3 * tau2 - 2 * tau) * self._x1p
        n = orthogonal_component(self._nhint, curve.r3normalize(xp))
        return x, n, xp

    @property
    def length(self):
        return self._len

class Cubic2DWithLinearHeight(Cubic):
    def __init__(self,
                 x0, x0prime,
                 x1, x1prime,
                 normal_hint,
                 height_index):
        self._hi = height_index
        self._h0 = x0[height_index]
        self._h1 = x1[height_index]
        x0 = np.copy(x0)
        x1 = np.copy(x1)
        x0[height_index] = 0.0
        x1[height_index] = 0.0
        super(Cubic2DWithLinearHeight, self).__init__(
                x0=x0, x0prime=x0prime,
                x1=x1, x1prime=x1prime,
                normal_hint=normal_hint)

    def get_conf(self, tau):
        x, n, xp = super(Cubic2DWithLinearHeight, self).get_conf(tau)
        x[self._hi] += self._h1 * tau + (1.0 - tau) * self._h0
        return x, n, xp

def RotationFromAxisAngle(axis, theta):
    return spla.expm(np.cross(np.eye(3), axis/spla.norm(axis)*theta))

class CircleXY(curve.Curve):

    def __init__(self, radius, x0rad, x1rad, h0, h1, origin=None, zero_boudary_dh=False):
        super(CircleXY, self).__init__()
        self._x0rad = x0rad
        self._x1rad = x1rad
        self._dxrad = x1rad - x0rad
        self._sign = 1.0 if x1rad > x0rad else -1.0
        self._h0 = h0
        self._h1 = h1
        self._R = radius
        self._is_zero_boundary_dh = zero_boudary_dh # h'(0) = h'(1) = 0, implies cubic h(tau)
        self._dh = 0 # For numeric_length
        '''
        self._len = curve.numeric_length(self, 1024)
        self._dh = (h1-h0)/self._len
        self._len = curve.numeric_length(self, 1024)
        self._dh = (h1-h0)/self._len
        '''
        self._len = np.linalg.norm([abs(self._dxrad) * self._R, h1-h0])

    def get_conf(self, tau):
        r = self._x0rad * (1 - tau) + self._x1rad * tau
        x = self._R * math.cos(r)
        y = self._R * math.sin(r)
        if not self._is_zero_boundary_dh:
            z = self._h0 * (1 - tau) + self._h1 * tau
        else:
            a =  2 * (self._h0 - self._h1)
            b = -3 * (self._h0 - self._h1)
            d = self._h0
            z = a * tau ** 3 + b * tau ** 2 + d
        p = np.array([x,y,z])
        # differential w.r.t. tau
        dx = -self._R * (self._dxrad) * math.sin(r)
        dy = self._R * (self._dxrad) * math.cos(r)
        if not self._is_zero_boundary_dh:
            dz = self._h1 - self._h0
        else:
            dz = 3 * a * tau ** 2 + 2 * b * tau
        # differential w.r.t. arc length
        t = np.array([dx,dy,dz]) / self._len
        #n = orthogonal_component(t, np.array([0,0,1]))
        n = orthogonal_component(np.array([0,0,1.0]), t)
        return p, n, t

    '''
    def get_conf(self, tau):
        r = self._x0rad * (1 - tau) + self._x1rad * tau
        h = self._h0 * (1 - tau) + self._h1 * tau
        nx = math.cos(r)
        ny = math.sin(r)
        x = self._R * nx
        y = self._R * ny
        z = h
        dx = -self._R * (self._dxrad) * math.sin(r)
        dy = self._R * (self._dxrad) * math.cos(r)
        tan = np.array([dx,dy,self._dh])
        normal = np.array([0.0, 0.0, 1.0])
        normal = orthogonal_component(normal, curve.r3normalize(tan))
        return np.array([x,y,z]), normal, tan
    '''

    @property
    def length(self):
        return self._len

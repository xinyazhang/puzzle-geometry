#!/usr/bin/env python3

from abc import abstractmethod
import abc
import numpy as np
import copy

ABC = abc.ABCMeta('ABC', (object,), {}) # compatible with Python 2 *and* 3

class Curve(ABC):
    def __init__(self):
        pass

    '''
    get_affine_conf: a warpper method that returns the affine point/vector

    Note: tangent MAY NOT be normalized
    '''
    def get_affine_conf(self, tau):
        medial,normal,tangent = self.get_conf(tau)
        medial = np.transpose(np.array([medial[0:3].tolist() + [1.0]]))
        normal = np.transpose(np.array([normal[0:3].tolist() + [0.0]]))
        tangent = np.transpose(np.array([tangent[0:3].tolist() + [0.0]]))
        return medial,normal,tangent

    '''
    get_conf
        get configuration at given tau

        INPUT
        tau in [0,1]

        RETURN
        (medial, normal, tangent) as 4D vector
        tangent SHOULD NOT be normalized to perseve more information
    '''
    @abstractmethod
    def get_conf(self, tau):
        pass

    '''
    get_length: get the arc length of the curve (segment)
    '''
    @property
    @abstractmethod
    def length(self):
        return -1

'''
r3normalize: normalize R^3 vector
'''
def r3normalize(vec):
    r3v = vec[:3]
    return r3v / np.linalg.norm(r3v)

def affine3normalize(vec):
    # print("affine3normalize {}".format(vec.shape))
    r3v = r3normalize(vec)
    # print("affine3normalize r3 {}".format(r3v.shape))
    return np.reshape(np.array(r3v.flatten().tolist() + [0.0]), vec.shape)

def numeric_length(curve, nsegments):
    pts = [curve.get_conf(tau)[0] for tau in np.linspace(0.0, 1.0, num=nsegments, endpoint=True)]
    norms = []
    for p0, p1 in zip(pts[:-1], pts[1:]):
        # print('numeric_length {} {}'.format(p0, p1))
        norms.append(np.linalg.norm(p0 - p1))
    return np.sum(norms)

'''
CAVEAT: CompositeCurve does not work very well ...
'''
class CompositeCurve(Curve):
    def __init__(self, member_curves):
        super(CompositeCurve, self).__init__()
        self._member_curvers = copy.deepcopy(member_curves)
        self._len = numeric_length(self, 1024)

    def get_conf(self, tau):
        x = np.zeros((3))
        xp = np.zeros((3))
        n = np.zeros((3))
        for c in self._member_curvers:
            x1, xp1, n1 = c.get_conf(tau)
            x += x1
            xp += xp1
            n += n1
        n = r3normalize(n)
        return x, n, xp

    @property
    def length(self):
        return self._len


import curve
import numpy as np
from math import pi,sin,cos

def _col_cross(x, y):
    xl = x.flatten().tolist()
    yl = y.flatten().tolist()
    # print('xl {} yl {}'.format(xl, yl))
    cross = np.cross(xl, yl)
    # print('cross {}'.format(cross))
    return np.reshape(cross, newshape=(3,1))

class CurveSegment:
    def __init__(self, c, segment_length):
        # print(c)
        # print("Curve len {} / {}".format(c.length, segment_length))
        nseg = c.length / segment_length
        taus = np.linspace(0.0, 1.0, num=nseg, endpoint=True)
        # print('taus {}'.format(taus))
        conf_tups = [c.get_affine_conf(tau) for tau in taus]
        self._medials = [tup[0] for tup in conf_tups]
        self._normals = [tup[1] for tup in conf_tups]
        self._tangents = [curve.affine3normalize(tup[2]) for tup in conf_tups]
        self._transform = np.eye(4)
        self._nsegments = len(taus)

    @property
    def nsegments(self):
        return self._nsegments

    def get_affine_matrix(self, index):
        t,n = self.tangent(index), self.normal(index)
        bn = np.array(_col_cross(t[:3], n[:3]).tolist() + [[0.0]]) # bi-normal
        # print('{} {} {}'.format(t[:3], n[:3], bn))
        o = self.medial(index)
        ret = np.concatenate([t, n, bn, o], axis=1)
        assert ret.shape == (4,4)
        return ret

    def set_transform(self, tx):
        self._transform = tx

    def get_transform(self):
        return self._transform

    transform = property(get_transform, set_transform)

    def medial(self, index):
        return self.transform.dot(self._medials[index])

    @property
    def all_medials(self):
        return [self.medial(i) for i in range(self.nsegments)]

    def normal(self, index):
        return self.transform.dot(self._normals[index])

    @property
    def all_normals(self):
        return [self.normal(i) for i in range(self.nsegments)]

    def tangent(self, index):
        return self.transform.dot(self._tangents[index])

    @property
    def all_tangents(self):
        return [self.tangent(i) for i in range(self.nsegments)]

def fuse_media_axis(curves, segment_length):
    css = [CurveSegment(curve, segment_length) for curve in curves]
    for prev,cs in zip(css[:-1], css[1:]):
        prev_end = prev.get_affine_matrix(-1)
        # print('prev end affine {}'.format(prev_end))
        curr_start = cs.get_affine_matrix(0)
        # print('curr start affine {}'.format(curr_start))
        tx = prev_end.dot(np.linalg.inv(curr_start))
        # print('set_transform {}'.format(tx))
        cs.set_transform(tx)
    return css

'''
libIGL standard: return (tess_level, 4) matrix, each row is an affine 3D point
'''
def circle_yz(rad, tess_level):
    ret = []
    for theta in np.linspace(0.0, 2. * pi, num=tess_level, endpoint=False):
        ret.append([0.0, rad * cos(theta), rad * sin(theta), 1.0])
    return np.array(ret)

def circle_faces(npts):
    curr_pts = np.linspace(0, npts, num=npts, endpoint=False, dtype=int)
    next_pts = curr_pts + npts
    faces = []
    for bl,br,tl,tr in zip(curr_pts[:-1], curr_pts[1:], next_pts[:-1], next_pts[1:]):
        faces.append(np.array([bl, br, tl], dtype=int))
        faces.append(np.array([br, tr, tl], dtype=int))
    bl,br = curr_pts[-1], curr_pts[0]
    tl,tr = next_pts[-1], next_pts[0]
    faces.append(np.array([bl, br, tl], dtype=int))
    faces.append(np.array([br, tr, tl], dtype=int))
    return np.array(faces, dtype=np.int32)

def vertices_mean(V):
    ret = np.mean(V, axis=0, keepdims=True)
    assert len(ret.shape) == 2
    assert ret.shape[0] == 1
    return ret

def pipe_seal(circle_vertex_base, circle_npts, center_vertex, flip):
    ring_vert_id = [circle_vertex_base+i for i in range(circle_npts)]
    if not flip:
        ring_vert_id = ring_vert_id[::-1]
    circular_vert_id = ring_vert_id + [ring_vert_id[0]]
    faces = []
    for v0, v1 in zip(circular_vert_id[:-1], circular_vert_id[1:]):
        faces.append(np.array([v0, v1, center_vertex], dtype=int))
    return np.array(faces, dtype=np.int32)

def build_pipe(css, radius, tess_level):
    # (4, tess_level) 
    template_circle_pts = np.transpose(circle_yz(radius, tess_level))
    circle_npts = template_circle_pts.shape[1]
    template_circle_faces = circle_faces(circle_npts)
    all_ring_verts = []
    all_ring_faces = []
    vert_id_base = 0
    for cs in css:
        print('cs.nsegments {}'.format(cs.nsegments))
        for i in range(cs.nsegments - 1):
            #print('affine {}'.format(cs.get_affine_matrix(i)))
            rings = np.transpose(cs.get_affine_matrix(i).dot(template_circle_pts))
            all_ring_verts.append(rings)
            all_ring_faces.append(template_circle_faces + vert_id_base)
            vert_id_base += circle_npts
    del all_ring_faces[-1] # Last ring connects to nothing
    center_of_first_ring = vertices_mean(all_ring_verts[0])
    center_of_last_ring = vertices_mean(all_ring_verts[-1])
    seal_of_first_ring = pipe_seal(circle_vertex_base=0,
                                   circle_npts=circle_npts,
                                   center_vertex=vert_id_base,
                                   flip=False)
    seal_of_last_ring = pipe_seal(circle_vertex_base=vert_id_base-circle_npts,
                                  circle_npts=circle_npts,
                                  center_vertex=vert_id_base+1,
                                  flip=True)
    V = np.concatenate(all_ring_verts+[center_of_first_ring, center_of_last_ring])
    F = np.concatenate(all_ring_faces+[seal_of_first_ring, seal_of_last_ring])
    return V, F

def fuse_curves(curves, radius, segment_length, circular_tess):
    css = fuse_media_axis(curves, segment_length)
    V, F = build_pipe(css, radius, circular_tess)

    return V, F

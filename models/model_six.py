from math import *
import numpy as np
import matplotlib.pyplot as plt
import pymesh
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D

r0 = 4.41/2
r = 19.66/2 + r0
r1 = 2*r
l1 = 12.48
l2 = 25.38
l3 = 15.85
#h0 = 7.41 + r0
h0 = 11.80 - 2 * r0
h = 3.12 + r0
theta = 4.0/9.0*pi
TQUAT_PI = 3.0/4.0 * pi
QUAT_PI_7 = 7.0/4.0 * pi

def get_medial_axis_normal(s):
    if s >=0 and s<=l3:
        x = sqrt(2)/2*(-r-s+l3)
        y = sqrt(2)/2*(r-s+l3)
        z = h0 - 0.1 * s
        vec_p = [x,y,z]
        vec_n = [sqrt(2)/2,-sqrt(2)/2,0]
        vec_t = [-sqrt(2)/2,-sqrt(2)/2, -0.1]

    elif s>l3 and s<=l3+QUAT_PI_7*r:
        x = r*cos((s-l3)/r+TQUAT_PI)
        y = r*sin((s-l3)/r+TQUAT_PI)
        b = l3+TQUAT_PI*r
        b1 = l3
        A = np.array([[b**3, b**2,b,1],[b1**3,b1**2,b1,1],[3*b**2,2*b,1,0],[3*b1**2,2*b1,1,0]])
        B = np.array([0,h0-0.1*l3,-0.1,0])
        a = np.linalg.solve(A,B)
        if s<=l3+3.0/4.0*pi*r:
            z = a[0]*s**3+a[1]*s**2+a[2]*s+a[3]
            t = 3*a[0]*s**2+2*a[1]*s+a[2]
        else:
            z = 0
            t = 0
        vec_p = [x,y,z]
        vec_n = [-cos((s-l3)/r+TQUAT_PI), -sin((s-l3)/r+TQUAT_PI),0]
        vec_t = [-sin((s-l3)/r+TQUAT_PI), cos((s-l3)/r+TQUAT_PI),t]
    elif s>l3+QUAT_PI_7*r and s<=l3+QUAT_PI_7*r+l2:
        x = -(s-l3-r*QUAT_PI_7)
        y = r
        z = 0
        vec_p = [x,y,z]
        vec_n = [0,-1,0]
        vec_t = [-1,0,0]
    elif s>l3+QUAT_PI_7*r+l2 and s<= l3+QUAT_PI_7*r+l2 + theta*r1:
        b1 = l3+QUAT_PI_7*r+l2
        b2 = l3+QUAT_PI_7*r+l2 + theta*r1 + l1
        x = -l2 -r1*sin((s-l2-l3-QUAT_PI_7*r)/r1)
        y = r1*cos((s-l2-l3-QUAT_PI_7*r)/r1)-r1+r
        A = np.array([[b1**2,b1,1],[b2**2,b2,1],[2*b1,1,0]])
        b = np.array([0,h,0])
        a = np.linalg.solve(A,b)
        z = a[0]*s**2 + a[1]*s +a[2]
        t = 2*a[0]*s+a[1]
        vec_p = [x,y,z]
        vec_n = [sin((s-l2-l3-QUAT_PI_7*r)/r1),-cos((s-l2-l3-QUAT_PI_7*r)/r1),0]
        vec_t = [-cos((s-l2-l3-QUAT_PI_7*r)/r1),-sin((s-l2-l3-QUAT_PI_7*r)/r1),t]
    else:
        s1 = s - (l3+QUAT_PI_7*r+l2 + theta*r1)
        x0 = -l2 - cos(pi/2-theta)*r1
        y0 = r1*sin(pi/2-theta)-r1+r
        x = x0-s1 * cos(theta)
        y = tan(theta)*x+(y0-tan(theta)*x0)
        b1 = l3+QUAT_PI_7*r+l2
        b2 = l3+QUAT_PI_7*r+l2 + theta*r1 + l1
        A = np.array([[b1**2,b1,1],[b2**2,b2,1],[2*b1,1,0]])
        b = np.array([0,h,0])
        a = np.linalg.solve(A,b)
        z = a[0]*s**2 + a[1]*s +a[2]
        t = 2*a[0]*s+a[1]
        vec_p = [x,y,z]
        vec_n = [sin(theta),-cos(theta),0]
        vec_t = [-cos(theta),-sin(theta),0]
    return np.array(vec_p), np.array(vec_n), np.array(vec_t)

def get_circle_pts(center_pt, normal, tangent, radius, pts_num):
    pts = []
    tangent = 1/np.linalg.norm(tangent)*tangent
    for i in range(pts_num):
        theta = 2*pi/pts_num*i
        v = cos(theta)*normal + sin(theta)*np.cross(tangent,normal) + (1-cos(theta))*np.dot(tangent,normal)*tangent
        pt = center_pt + radius*v
        pts.append([pt[0],pt[1],pt[2]])
    return np.array(pts)

def get_surface(num_loops, num_theta):
    s1 = l3;
    s2 = s1 + TQUAT_PI*r
    s3 = s1 + QUAT_PI_7*r
    s4 = s3 + l2
    s5 = s4 + theta*r1
    s6 = s5 + l1
    inter_s = [s1,s2,s3,s4,s5,s6]
    len_s = [s1, s2-s1,s3-s2,s4-s3,s5-s4, s6-s5]
    num_s = []
    for l in len_s:
        num_s.append(int(l/s6 * num_loops))
    cur_loop = 0
    s = np.linspace(0, inter_s[0],num_s[0])
    vec_p, vec_n, vec_t = get_medial_axis_normal(s[0])
    vertices = get_circle_pts(vec_p,vec_n,vec_t, r0, num_theta)
    faces = []
    cur_loop += 1
    for i in range(1, s.shape[0]-1):
        vec_p, vec_n, vec_t = get_medial_axis_normal(s[i])
        loop_pts = get_circle_pts(vec_p,vec_n,vec_t, r0, num_theta)
        vertices = np.concatenate((vertices, loop_pts))
        for k in range(num_theta):
            faces.append([(cur_loop-1)*num_theta+k, (cur_loop-1)*num_theta + (k+1)%num_theta, cur_loop*num_theta + (k+1)%num_theta])
            faces.append([cur_loop*num_theta + (k+1)%num_theta, cur_loop*num_theta+k, (cur_loop-1)*num_theta+k])
        cur_loop += 1

    for i in range(len(inter_s)-1):
        s = np.linspace(inter_s[i],inter_s[i+1],num_s[i+1])
        for j in range(s.shape[0]-1):
            vec_p, vec_n, vec_t = get_medial_axis_normal(s[j])
            loop_pts = get_circle_pts(vec_p,vec_n,vec_t, r0, num_theta)
            vertices = np.concatenate((vertices, loop_pts))
            for k in range(num_theta):
                faces.append([(cur_loop-1)*num_theta+k, (cur_loop-1)*num_theta + (k+1)%num_theta, cur_loop*num_theta + (k+1)%num_theta])
                faces.append([cur_loop*num_theta + (k+1)%num_theta, cur_loop*num_theta+k, (cur_loop-1)*num_theta+k])
            cur_loop += 1

    faces =  np.array(faces)
    mesh =  pymesh.form_mesh(vertices, faces)
    pymesh.save_mesh("six.obj", mesh, use_double =True)

def main():
    get_surface(200,50)
    # r0 = 4.41/2
    # r = 19.66/2
    # r1 = 2*r
    # l1 = 12.48
    # l2 = 25.38
    # l3 = 15.85
    # z0 = 7.41
    # h = 3.12 + r0
    # theta = 4/9*pi
    # smax = l3+QUAT_PI_7*r+l2 + theta*r1 + l1
    # s = np.linspace(0, smax, 200)
    # center_pts = []
    # x = []
    # y = []
    # z = []
    # for i in range(s.shape[0]):
    #     vec_p, vec_n, vec_t = get_medial_axis_normal(s[i])
    #     center_pts.append([vec_p[0],vec_p[1],vec_p[2]])
    #     x.append(vec_p[0])
    #     y.append(vec_p[1])
    #     z.append(vec_p[2])
    # x = np.array(x)
    # y = np.array(y)
    # z = np.array(z)

    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # ax.plot(x,y,z)
    # ax.set_aspect('equal')
    # plt.show()

if __name__ == '__main__':
    main()

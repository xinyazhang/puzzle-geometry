from math import *
import numpy as np
import matplotlib.pyplot as plt
import pymesh
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D

r0 = 4.52/2
r1 = 12.28/2+r0
r2 = 2*r0

l1 = 23.13
l2 = 36.18
l3 = 15.13
l4 = 20.32

h1 = 12.65
h2 = 20.08
sc = 7*r0
delta_x = 4*r0
delta_y = 0

z1 = 9.89
z2 = 1.7*r0
s_z2 = 12.37

z3 = 0.5*r0
z4 = r0

theta_1 = 2*acos(h1/l3)
theta_2 = acos(h2/l4)


x1 = -(l2 + r2*sin(theta_1)) - l3*sin(theta_1/2)
y1 = r1 - r2 + r2*cos(theta_1) - l3*cos(theta_1/2)

start = 0
end = 3.0/2.0*pi*r1+l1+l2 + r2*theta_1+l3+sc + l4

HALF_PI = 1.0/2.0 * pi
HALF_PI_3 = 3.0/2.0 * pi

def get_medial_axis_normal(s):
    vec_p = [0,0,0]
    vec_t = [1,0,0]
    vec_n = [0,-1,0]
    A = np.array([[0,1],[s_z2,1]])
    b = np.array([z1,z2])
    lin_coef = np.linalg.solve(A,b)
    A = np.array([[HALF_PI_3*r1+l1+l2 + r2*theta_1+l3+sc,1],[HALF_PI_3*r1+l1+l2 + r2*theta_1+l3+sc+l4,1]])
    b = np.array([z3,z4])
    lin_coef_1 = np.linalg.solve(A,b)
    x1 = -(l2 + r2*sin(theta_1)) - l3*sin(theta_1/2)
    y1 = r1 - r2 + r2*cos(theta_1) - l3*cos(theta_1/2)
    cubic_coef_x = cubic_hemite(0,sc,x1,-sin(theta_1/2),x1-delta_x,-sin(theta_2))
    cubic_coef_y = cubic_hemite(0,sc,y1,-cos(theta_1/2),y1-delta_y,cos(theta_2))
    A = np.array([[l1**2,l1,1],[(HALF_PI*r1+l1)**2,HALF_PI*r1+l1,1],[2*(HALF_PI*r1+l1),1,0]])
    b = np.array([z2,0,0])
    quad_coef = np.linalg.solve(A,b)
    if s>=0 and s<l1:
        x = -r1
        y = l1-s
        k = 2*quad_coef[0]*l1 + quad_coef[1]
        z = k*(s-l1)+z2
        vec_p = [x,y,z]
        vec_t = [0,-1,k]
        vec_n = [1,0,0]
    elif s>=l1 and s<HALF_PI_3*r1+l1:
        theta = (s-l1)/r1
        x = r1*cos(pi+theta)
        y = r1*sin(pi+theta)
        z = 0
        t = 0
        if s<= HALF_PI*r1+l1:
            z = quad_coef[0]*s**2 + quad_coef[1]*s + quad_coef[2]
            t = 2*quad_coef[0]*s + quad_coef[1]
        vec_p = [x,y,z]
        vec_t = [-sin(pi+theta),cos(pi+theta),t]
        vec_n = [-cos(pi+theta), -sin(pi+theta),0]
    elif s>=HALF_PI_3*r1+l1 and s<HALF_PI_3*r1+l1+l2:
        s1 = s - (HALF_PI_3*r1+l1)
        x = -s1
        y = r1
        z = 0
        vec_p = [x,y,z]
        vec_t = [-1,0,0]
        vec_n = [0,-1,0]
    elif s>=HALF_PI_3*r1+l1+l2 and s<HALF_PI_3*r1+l1+l2+r2*theta_1:
        theta = (s - (HALF_PI_3*r1+l1+l2))/r2
        x = -(l2 + r2*sin(theta))
        y = r1 - r2 + r2*cos(theta)
        z = 0
        vec_p = [x,y,0]
        vec_t = [-cos(theta), -sin(theta),0]
        vec_n = [sin(theta), -cos(theta),0]
    elif s>=HALF_PI_3*r1+l1+l2+r2*theta_1  and s<HALF_PI_3*r1+l1+l2+r2*theta_1+l3:
        s1 = HALF_PI_3*r1+l1+l2+r2*theta_1
        s1 = s - s1
        theta = theta_1
        x = -(l2 + r2*sin(theta))
        y = r1 - r2 + r2*cos(theta)
        x = x - s1*sin(theta_1/2)
        y = y - s1*cos(theta_1/2)
        z = 0
        vec_p =[x,y,0]
        vec_t = [-sin(theta_1/2),-cos(theta_1/2),0]
        vec_n = [cos(theta_1/2), -sin(theta_1/2),0]
    elif s>=HALF_PI_3*r1+l1+l2+r2*theta_1+l3 and s<HALF_PI_3*r1+l1+l2+r2*theta_1+l3+sc:
        s1 = s - (HALF_PI_3*r1+l1+l2+r2*theta_1+l3)
        x = cubic_coef_x[0]*s1**3+cubic_coef_x[1]*s1**2+cubic_coef_x[2]*s1+cubic_coef_x[3]
        y = cubic_coef_y[0]*s1**3+cubic_coef_y[1]*s1**2+cubic_coef_y[2]*s1+cubic_coef_y[3]
        cubic_coef = cubic_hemite(HALF_PI_3*r1+l1+l2+r2*theta_1+l3,HALF_PI_3*r1+l1+l2+r2*theta_1+l3+sc,\
        0,0,\
        lin_coef_1[0]*(HALF_PI_3*r1+l1+l2+r2*theta_1+l3+sc)+lin_coef_1[1], lin_coef_1[0])
        z = cubic_coef[0]*s**3 + cubic_coef[1]*s**2 + cubic_coef[2]*s + cubic_coef[3]
        vec_p = [x,y,z]
        vec_t = [3*cubic_coef_x[0]*s1**2 + 2*cubic_coef_x[1]*s1 + cubic_coef_x[2], 3*cubic_coef_y[0]*s1**2 + 2*cubic_coef_y[1]*s1 + cubic_coef_y[2], 3*cubic_coef[0]*s**2 + 2*cubic_coef[1]*s + cubic_coef[2]]
        vec_n = [-vec_t[1],vec_t[0],0]
    elif s<=HALF_PI_3*r1+l1+l2 + r2*theta_1+l3+sc + l4:
        s1 = s - (HALF_PI_3*r1+l1+l2 + r2*theta_1+l3+sc)
        x = x1 - delta_x - s1*sin(theta_2)
        y = y1 - delta_y + s1*cos(theta_2)
        z = lin_coef_1[0]*s + lin_coef_1[1]
        vec_p = [x,y,z]
        vec_t = [-sin(theta_2), cos(theta_2), lin_coef_1[0]]
        vec_n = [-cos(theta_2),-sin(theta_2),0]
    vec_p = np.array(vec_p)
    vec_t = np.array(vec_t)
    vec_n = np.array(vec_n)
    vec_n = 1/np.linalg.norm(vec_n)*vec_n
    return vec_p, vec_n, vec_t

def cubic_hemite(x1,x2,f1,df1,f2,df2):
    A = np.array([[x1**3,x1**2,x1,1],[3*x1**2,2*x1,1,0],[x2**3,x2**2,x2,1],[3*x2**2,2*x2,1,0]])
    b = np.array([f1,df1,f2,df2])
    return np.linalg.solve(A,b)

def get_circle_pts(center_pt, normal, tangent, radius, pts_num):
    pts = []
    tangent = 1/np.linalg.norm(tangent)*tangent
    for i in range(pts_num):
        theta = 2*pi/pts_num*i
        v = cos(theta)*normal + sin(theta)*np.cross(tangent,normal) + (1-cos(theta))*np.dot(tangent,normal)*tangent
        pt = center_pt + radius*v
        pts.append([pt[0],pt[1],pt[2]])
    return np.array(pts)

def get_surface(num_loops, num_theta):
    s = np.linspace(start,end,num_loops)
    cur_loop = 0
    vec_p, vec_n, vec_t = get_medial_axis_normal(s[0])
    vertices = get_circle_pts(vec_p,vec_n,vec_t, r0, num_theta)
    faces = []
    cur_loop += 1
    for i in range(1, s.shape[0]-1):
        vec_p, vec_n, vec_t = get_medial_axis_normal(s[i])
        loop_pts = get_circle_pts(vec_p,vec_n,vec_t, r0, num_theta)
        vertices = np.concatenate((vertices, loop_pts))
        for k in range(num_theta):
            faces.append([(cur_loop-1)*num_theta+k, (cur_loop-1)*num_theta + (k+1)%num_theta, cur_loop*num_theta + (k+1)%num_theta])
            faces.append([cur_loop*num_theta + (k+1)%num_theta, cur_loop*num_theta+k, (cur_loop-1)*num_theta+k])
        cur_loop += 1
    faces =  np.array(faces)
    mesh =  pymesh.form_mesh(vertices, faces)
    pymesh.save_mesh("window_catch.obj", mesh, use_double =True)

def main():
    get_surface(200,50)
    # smax =  HALF_PI_3*r1+l1+l2 + r2*theta_1+l3+sc+l4
    # s = np.linspace(0, smax, 200)
    # x = []
    # y = []
    # z = []
    # for i in range(s.shape[0]):
    #     vec_p = get_medial_axis_normal(s[i])
    #     if vec_p.shape[0] == 3:
    #         x.append(vec_p[0])
    #         y.append(vec_p[1])
    #         z.append(vec_p[2])
    # x = np.array(x)
    # y = np.array(y)
    # z = np.array(z)



    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # ax.plot(x,y,z)
    # ax.set_aspect('equal')
    # plt.show()
    # s = np.linspace(0, smax, 200)
    # x = []
    # y = []
    # for i in range(s.shape[0]):
    #     vec_p = get_medial_axis_normal(s[i])
    #     if vec_p.shape[0] == 2:
    #         x.append(vec_p[0])
    #         y.append(vec_p[1])
    # x = np.array(x)
    # y = np.array(y)



    # fig = plt.figure()
    # plt.plot(x,y)
    # plt.axis('equal')
    # plt.show()


main()

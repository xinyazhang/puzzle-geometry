from math import *
import numpy as np
import matplotlib.pyplot as plt
import pymesh
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D

l1 = 20.32
l2 = 25.70
l3 = 10.83
r0 = 2.32
r1 = 19.56/2 + r0
r2 = 10.40/2 + r0

h1 = 9.55 - r0
h2 =  13.23 - r0
h3 = 17.62 - r0

HALF_PI_3 = 3.0 / 2.0 * pi
QUAT_PI_3 = 3.0 / 4.0 * pi


def get_medial_axis_normal(s):
    s1 = l1 + QUAT_PI_3*r1
    s2 = l1 + HALF_PI_3*r1
    s3 = l1 + HALF_PI_3*r1 + l2
    s4 = s3 + QUAT_PI_3*r2
    s5 = s4 + QUAT_PI_3*r2

    A = np.array([[s2,1],[s3,1]])
    b = np.array([h1,h2])
    lin_coef = np.linalg.solve(A,b)
    a = lin_coef[0]
    cubic_coef_1 = cubic_hemite(s1,s2,0,0,h1,a)
    cubic_coef_2 = cubic_hemite(s3,s4,h2,a,h3,0)

    vec_p = []
    if s>=0 and s <l1:
        y = sqrt(2)/2*(s-l1+r1)
        x = y -sqrt(2)*r1
        vec_p = [x,y,0]
        vec_t = [sqrt(2)/2,sqrt(2)/2,0]
        vec_n = [-sqrt(2)/2,sqrt(2)/2,0]
    elif s>=l1 and s<l1+HALF_PI_3*r1:
        theta = (s-l1)/r1
        x = r1*cos(QUAT_PI_3-theta)
        y = r1*sin(QUAT_PI_3-theta)
        z = 0
        t = 0
        if s >= l1+QUAT_PI_3*r1:
            z = cubic_coef_1[0]*s**3+cubic_coef_1[1]*s**2+cubic_coef_1[2]*s+cubic_coef_1[3]
            t = 3*cubic_coef_1[0]*s**2+2*cubic_coef_1[1]*s+cubic_coef_1[2]
        vec_p = [x,y,z]
        vec_t = [sin(QUAT_PI_3-theta), -cos(QUAT_PI_3-theta),t]
        vec_n = [cos(QUAT_PI_3-theta),sin(QUAT_PI_3-theta),0]
    elif s>=l1+HALF_PI_3*r1 and s<l1+l2+HALF_PI_3*r1:
        s1 = s - (l1+HALF_PI_3*r1)
        y = sqrt(2)/2*(s1-r1)
        x = -y-sqrt(2)*r1
        z = lin_coef[0]*s + lin_coef[1]
        t = lin_coef[0]
        vec_p = [x,y,z]
        vec_t = [-sqrt(2)/2,sqrt(2)/2,t]
        vec_n = [-sqrt(2)/2,-sqrt(2)/2,0]
    elif s>= l1+l2+HALF_PI_3*r1 and s<l1+l2+HALF_PI_3*(r1+r2):
        theta = (s - (l1+l2+HALF_PI_3*r1))/r2
        y0 = (l2-r1-r2)/sqrt(2)
        x0 = -sqrt(2)*r2 - y0 -sqrt(2)*r1
        x = x0 + r2*cos(theta+pi/4)
        y = y0 + r2*sin(theta+pi/4)
        t = 0
        if s <= l1+l2+HALF_PI_3*r1+QUAT_PI_3*r2:
            z = cubic_coef_2[0]*s**3+cubic_coef_2[1]*s**2+cubic_coef_2[2]*s+cubic_coef_2[3]
            t = 3*cubic_coef_2[0]*s**2+2*cubic_coef_2[1]*s+cubic_coef_2[2]
        else:
            z = h3
        vec_p = [x,y,z]
        vec_t = [-sin(theta+pi/4), cos(theta+pi/4), t]
        vec_n = [-cos(theta+pi/4),-sin(theta+pi/4),0]
    else:
        s1 = s - (l1+l2+HALF_PI_3*(r1+r2))
        theta = 3*pi/2
        y0 = (l2-r1-r2)/sqrt(2)
        x0 = -sqrt(2)*r2 - y0 -sqrt(2)*r1
        x1 = x0 + r2*cos(theta+pi/4)
        y1 = y0 + r2*sin(theta+pi/4)
        x = x1 + s1/sqrt(2)
        y = y1 + s1/sqrt(2)
        z = h3
        vec_p = [x,y,z]
        vec_t = [sqrt(2)/2,sqrt(2)/2,0]
        vec_n = [-sqrt(2)/2,sqrt(2)/2,0]
    return np.array(vec_p), np.array(vec_n), np.array(vec_t)

def cubic_hemite(x1,x2,f1,df1,f2,df2):
    A = np.array([[x1**3,x1**2,x1,1],[3*x1**2,2*x1,1,0],[x2**3,x2**2,x2,1],[3*x2**2,2*x2,1,0]])
    b = np.array([f1,df1,f2,df2])
    return np.linalg.solve(A,b)

def get_circle_pts(center_pt, normal, tangent, radius, pts_num):
    pts = []
    tangent = 1/np.linalg.norm(tangent)*tangent
    for i in range(pts_num):
        theta = 2*pi/pts_num*i
        v = cos(theta)*normal + sin(theta)*np.cross(tangent,normal) + (1-cos(theta))*np.dot(tangent,normal)*tangent
        pt = center_pt + radius*v
        pts.append([pt[0],pt[1],pt[2]])
    return np.array(pts)

def get_surface(num_loops, num_theta):
    start = 0
    end = l1+l2+HALF_PI_3*(r1+r2)+l3
    s = np.linspace(start,end,num_loops)
    cur_loop = 0
    vec_p, vec_n, vec_t = get_medial_axis_normal(s[0])
    vertices = get_circle_pts(vec_p,vec_n,vec_t, r0, num_theta)
    faces = []
    cur_loop += 1
    for i in range(1, s.shape[0]-1):
        vec_p, vec_n, vec_t = get_medial_axis_normal(s[i])
        loop_pts = get_circle_pts(vec_p,vec_n,vec_t, r0, num_theta)
        vertices = np.concatenate((vertices, loop_pts))
        for k in range(num_theta):
            faces.append([(cur_loop-1)*num_theta+k, (cur_loop-1)*num_theta + (k+1)%num_theta, cur_loop*num_theta + (k+1)%num_theta])
            faces.append([cur_loop*num_theta + (k+1)%num_theta, cur_loop*num_theta+k, (cur_loop-1)*num_theta+k])
        cur_loop += 1
    faces =  np.array(faces)
    mesh =  pymesh.form_mesh(vertices, faces)
    pymesh.save_mesh("eight.obj", mesh, use_double =True)

def main():
    get_surface(200,50)
    # smax =  l1+l2+HALF_PI_3*(r1+r2)+l3
    # s = np.linspace(0, smax, 200)
    # x = []
    # y = []
    # z = []
    # for i in range(s.shape[0]):
    #     vec_p = get_medial_axis_normal(s[i])
    #     if vec_p.shape[0] == 3:
    #         x.append(vec_p[0])
    #         y.append(vec_p[1])
    #         z.append(vec_p[2])
    # x = np.array(x)
    # y = np.array(y)
    # z = np.array(z)



    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # ax.plot(x,y,z)
    # ax.set_aspect('equal')
    # plt.show()

if __name__ == '__main__':
    main()
